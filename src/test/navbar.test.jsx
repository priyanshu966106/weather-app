import * as React from 'react'
import ReactShallowRenderer from 'react-test-renderer/shallow'
import { Navbartop } from '../components/navbar'

test('navbar', () => {
    const renderer = new ReactShallowRenderer();
    renderer.render(< Navbartop />);
    expect(renderer.getRenderOutput()).toMatchSnapshot();

});
