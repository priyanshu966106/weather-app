import * as React from 'react'
//import * as ReactDOM from 'react-dom';
import { mount } from 'enzyme';
import * as enzyme from 'enzyme';
import { App } from '../components/App'
import { WebFront } from '../components/WebFront'
import { Pnp } from '../components/Pnp'
import { Barea } from '../components/Barea'
import { MemoryRouter } from 'react-router-dom'
import * as Adapter from 'enzyme-adapter-react-16';
enzyme.configure({ adapter: new Adapter() });



test('invalid path should redirect to 404', () => {
    const wrapper = mount(
        <MemoryRouter initialEntries={['/pin', '/jjkl']}>
            <App />
        </MemoryRouter >
    );
    expect(wrapper.find(WebFront)).toHaveLength(1);
    expect(wrapper.find(Pnp)).toHaveLength(0);
    expect(wrapper.find(Barea)).toHaveLength(0);
});
