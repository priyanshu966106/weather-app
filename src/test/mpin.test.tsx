import * as React from 'react';
import * as enzyme from 'enzyme';
jest.mock("../components/service/getapi")
import { shallow } from 'enzyme'

import * as Adapter from 'enzyme-adapter-react-16';
enzyme.configure({ adapter: new Adapter() });
import { PinPage } from '../components/pinpage'

test('pin test', () => {
    const wrapper = shallow(<PinPage />)
    const value = "827009"

    wrapper.find('input').at(0).simulate('change', {
        target: {
            value
        }
    });


    wrapper.find('Button').at(0).simulate('click', {

    });
    expect(wrapper.state('errMsg')).toBe("");
    expect(wrapper.state('showWeaStat')).toBe(false);
    expect(wrapper.state('showAlert')).toBe(false);
    setTimeout(() => {
        wrapper.update();
        expect(wrapper.state('reps')).toBe("");
        expect(wrapper.state('icon')).toBe("");



    });

});