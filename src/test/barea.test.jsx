import * as React from 'react'
import ReactShallowRenderer from 'react-test-renderer/shallow'
import { Barea } from '../components/Barea'

test('barea should render', () => {
    const renderer = new ReactShallowRenderer();
    renderer.render(<Barea />);
    expect(renderer.getRenderOutput()).toMatchSnapshot();

});
