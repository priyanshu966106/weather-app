
const add = (a: number, b: number) => a + b;

it('renders without crashing', () => {
  const result = add(3, 4);
  expect(result).toBe(7);
});



