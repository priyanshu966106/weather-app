import * as React from 'react';
import { shallow } from 'enzyme'
import { DateRen } from '../components/DateRen'
import * as enzyme from 'enzyme';
import * as Adapter from 'enzyme-adapter-react-16';
enzyme.configure({ adapter: new Adapter() });

test('DataRen Testing', () => {

    const wrapper = shallow(<DateRen place="delhi" imgUrl="http://test.png" weatherCondition="rainy" />)

    expect(wrapper.find('p').at(0).text()).toBe(' Place: delhi')
    expect(wrapper.find('p').at(1).text()).toBe('rainy')

})