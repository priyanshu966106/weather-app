import * as React from 'react'
import ReactShallowRenderer from 'react-test-renderer/shallow'
import { WebFront } from '../components/WebFront'
import * as enzyme from 'enzyme';
import { shallow } from 'enzyme'
import * as Adapter from 'enzyme-adapter-react-16';
enzyme.configure({ adapter: new Adapter() });

test('WeFront', () => {
    const renderer = new ReactShallowRenderer();
    renderer.render(<  WebFront />);
    expect(renderer.getRenderOutput()).toMatchSnapshot();
});

test('pin test', () => {
    const wrapper = shallow(<WebFront />)
});

