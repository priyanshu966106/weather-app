import * as React from 'react';
import { shallow } from 'enzyme'
import * as enzyme from 'enzyme';
import { FormPage } from '../components/FormPage';
import * as Adapter from 'enzyme-adapter-react-16';
enzyme.configure({ adapter: new Adapter() });


test('state testing it is ', () => {
    const value = 'delhi'
    const wrapper = shallow(<FormPage />)
    wrapper.find('input').at(0).simulate('change', {
        target: {
            value
        }
    });
    expect(wrapper.state('country')).toBe("delhi");

})

test('when button click without value ', () => {

    const wrapper = shallow(<FormPage />)

    wrapper.find('Button').at(0).simulate('click', {

    });
    expect(wrapper.state('errMsg')).toBe("Place can't be blank");

})

test('when button click without value', () => {

    const wrapper = shallow(<FormPage />)

    wrapper.find('Button').at(0).simulate('click', {

    });
    expect(wrapper.state('errMsg')).toBe("Place can't be blank");
    expect(wrapper.state('showWeaStat')).toBe(false);
    expect(wrapper.state('showAlert')).toBe(true);
    expect(wrapper.state('reps')).toBe("");
    expect(wrapper.state('icon')).toBe("");
})

test('when button is clicked', () => {

    const wrapper = shallow(<FormPage />);
    const value = 'delhi'

    wrapper.find('input').at(0).simulate('change', {
        target: {
            value
        }
    });

    wrapper.find('Button').at(0).simulate('click', {

    });
    expect(wrapper.state('errMsg')).toBe("");
    expect(wrapper.state('showWeaStat')).toBe(false);
    expect(wrapper.state('showAlert')).toBe(false);
    expect(wrapper.state('reps')).toBe("");
    expect(wrapper.state('icon')).toBe("");
})


