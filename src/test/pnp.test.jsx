import* as React from 'react'
import ReactShallowRenderer from 'react-test-renderer/shallow'
import {Pnp} from '../components/Pnp'

test('page not found should render', ()=>{
const renderer=new ReactShallowRenderer();
renderer.render(<Pnp/>);
expect(renderer.getRenderOutput()).toMatchSnapshot();

});
