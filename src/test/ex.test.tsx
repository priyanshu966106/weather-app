import * as React from 'react';
import { shallow } from 'enzyme'
import { Aino } from '../components/Aino';
import * as enzyme from 'enzyme';
import { PinPage } from '../components/pinpage';
import * as Adapter from 'enzyme-adapter-react-16';
enzyme.configure({ adapter: new Adapter() });

test('testing data rendering', () => {
    const wrapper = shallow(<Aino />);
    //expect(wrapper.find('h1').text()).toBe("  Weather app")
    expect(wrapper).toMatchSnapshot()
}
)

test('state testing it is ', () => {
    const value = 'new dess'
    const wrapper = shallow(<PinPage />)
    wrapper.find('input').at(0).simulate('change', {
        target: {
            value
        }
    });
    expect(wrapper.state('reps')).toBe("");
})
