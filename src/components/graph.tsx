import * as React from 'react'

const Chart = require('react-google-charts').Chart;

let data:any=[
    ['Day', 'Temp'], 
    [212, 1], 
    [402, 2],
    [500, 3]
    

]



export class GraphCon extends React.Component {

    render() {
        return (
            <div>
                <Chart
          chartType="LineChart"
          data={data}
          options={{}}
          graph_id="ScatterChart"
          width="100%"
          height="400px"
          legend_toggle={true}
        />

            </div>

        )
    }
}