import * as React from 'react'
import {BrowserRouter,Route,Switch} from 'react-router-dom'
import {Pnp} from './Pnp'
import {Navbartop} from './navbar'
import {Testp} from './testp'
import {Barea} from './Barea'
import {WebFront}from './WebFront'



export class App extends React.Component {

    render() {
        return (
            <div>
                <BrowserRouter>
                    <div>

                        <Navbartop />
                        <Switch>

                            <Route path="/" component={WebFront} exact={true} />
                            <Route path="/area" component={Barea} excat={true} />
                            <Route path="/pin" component={Testp} excat={true} />
                            <Route component={Pnp} />
                        
                        </Switch>

                    </div>

                </BrowserRouter>
               
            </div>
        )
    }
}