import * as React from 'react';
import { Button, Form } from 'react-bootstrap'
import { DateRen } from './DateRen';
import { Web_loader } from './Web_loader';
import {ale} from './service/alert'
import { ToastContainer } from 'react-toastify';
import { Link } from 'react-router-dom'


import {validate} from 'validate.js'
 var constrains={
    place: {
        presence: true,
        length: {
            minimum: 3,
            message: "must be at least 3 characters"
          }
        
       
    }
}


export interface IFrontPageState {
    country: any;
    reps: any;
    icon: string;
    showWeaStat: boolean
    showLoader: boolean
    showAlert: boolean
    errMsg:string
}

export class FormPage extends React.Component<any, IFrontPageState>{

    constructor(props: any) {
        super(props)
        this.state = {
            country: null,
            reps: '',
            icon: '',
            showWeaStat: false,
            showLoader: false,
            showAlert: false,
            errMsg:''
        }
    }

    handleChange = (event: any) => {
        this.setState({ country: event.target.value });
        this.setState({ showAlert: false });
        this.setState({ showLoader: false });
        this.setState({ showWeaStat: false });
    }

    showState = async() => {
        console.log("called");
        console.log("validate",validate({place:this.state.country}, constrains));
        this.setState({ showLoader: true })
        if (!validate({place:this.state.country}, constrains)) {
            this.setState({ showAlert: false })
            let cstring: string = this.state.country;
            const AppId: string = "f7aa90d5e6f9b6337399b73f70e00b0a";
            const url: string = `https://api.openweathermap.org/data/2.5/weather?q=${cstring}&APPID=${AppId}`;
            console.log('url', url);
          try{
           await fetch(url, {
                method: 'GET'

            }).then(response => response.json())
            
                .then(json => this.setState({ reps: json.weather[0].description, icon: json.weather[0].icon }));
            this.setState({ showLoader: false });
            this.setState({ showWeaStat: true })}
            catch{
              ale("unrecognised Area")
            }
        } else {
            this.setState({ showAlert: true })
            let err=validate({place:this.state.country}, constrains)
            this.setState({errMsg:err.place[0]})
            
        }
    }

    public render() {

        return (

            <div className="afpage">
            <ToastContainer/>
                {
                    this.state.showAlert ? ale(this.state.errMsg) : <div />
                }
                <Form>
                    <input type="text" onChange={this.handleChange} placeholder="Enter Area" />
                    <br /><br />
                    <Button bsStyle="primary" onClick={this.showState} >
                        Find weather
        </Button>{' '}<Link to="/"><Button bsStyle="danger">
                        Back
        </Button></Link><br /><br />

                </Form>
                {this.state.showWeaStat ?
                    <DateRen imgUrl={`http://openweathermap.org/img/w/${this.state.icon}.png`} weatherCondition={this.state.reps} place={this.state.country} />
                    :
                    <div><Web_loader showLoaderp={this.state.showLoader} /> </div>
                }

            </div>

        )
    }
}




