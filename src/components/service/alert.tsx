import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

export const ale=(err:string)=>{

    toast.error(err, {
        position: toast.POSITION.TOP_RIGHT });

}