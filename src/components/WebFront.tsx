import * as React from 'react';
import{Button} from 'react-bootstrap';
import {Link} from 'react-router-dom'
import {GraphCon}from './graph'


import {Aino} from './Aino'
import './WebF.css'


export class WebFront extends React.Component<{}>{
    
    
   public render() :JSX.Element{
        return(
           
           <div className="fc"> 
            <Aino/>
            <h3> Welcome Click On button to get Started</h3>
            <br/>
           <Link to="/area"><Button bsStyle="danger" >
             By Area
            </Button></Link>{' '}
            
            <Link to="/pin"> <Button bsStyle="danger" >
             By Pincode
            </Button></Link>
            <GraphCon/>
            
            
            </div>
        )
    }
}