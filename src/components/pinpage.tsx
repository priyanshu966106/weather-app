import * as React from 'react';
import { Button, Form } from 'react-bootstrap'
import { DateRen } from './DateRen';
import { Web_loader } from './Web_loader';
import { Link } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';
import {ale} from './service/alert'
import {validate} from 'validate.js'
import getapi from './service/getapi'


var constrains={
    pin :{
        presence: true,
        length: {
            minimum: 6,
            maximum:6,
            message: "must be 6 characters"
          },
          numericality: {
            onlyInteger: true,
            message:" characters must be integers"
          }  
    }

}


export interface IPinPageState {
    zip: number;
    reps: any;
    place:string;
    icon: string;
    showWeaStat: boolean
    showLoader: boolean
    showAlert: boolean
    errMsg:string
}

export class PinPage extends React.Component<any, IPinPageState>{

    constructor(props: any) {
        super(props)
        this.state = {
            zip: 0,
            reps: '',
            icon: '',
            place:'',
            showWeaStat: false,
            showLoader: false,
            showAlert: false,
            errMsg:''
        }
    }

    handleChange = (event: any) => {
        this.setState({ zip: event.target.value });
        this.setState({ showAlert: false })
    }

    

    showState = async () => {
        console.log("validate",validate({pin:this.state.zip}, constrains))
        if (!validate({pin:this.state.zip}, constrains)) {
            this.setState({ showAlert: false })
            let cstring: number = this.state.zip;
            const AppId: string = "f7aa90d5e6f9b6337399b73f70e00b0a";
            const url: string = `https://api.openweathermap.org/data/2.5/weather?q=${cstring}&APPID=${AppId}`;
            console.log('url', url);
            console.log("called");
            this.setState({ showLoader: true });
            const apidata: any = await getapi(url);
            try{
            this.setState({reps:apidata.data.weather[0].description})
            this.setState({icon:apidata.data.weather[0].icon})
            this.setState({place:apidata.data.name})
            this.setState({ showWeaStat: true })
            }
            catch{
                ale("Pin not found")
                this.setState({ showWeaStat: false })
            }
            console.log('function call', apidata);
           

        } else {
            this.setState({ showAlert: true })
         let err=validate({pin:this.state.zip}, constrains)
         this.setState({errMsg:err.pin[0]})
         

        }
        this.setState({ showLoader: false })
        
    }

    public render() {

        return (

            <div className="afpage">
            <ToastContainer />
                {
                    this.state.showAlert ? ale(this.state.errMsg) : <div />
                }
                <Form>
                    <input type="text" onChange={this.handleChange} placeholder="Enter Pincode" />
                    <br /><br />
                    <Button bsStyle="primary" onClick={this.showState}>
                        Find weather
        </Button>{' '}<Link to="/"><Button bsStyle="danger">
                        Back
        </Button></Link><br /><br />

                </Form>
                {this.state.showWeaStat ?
                    <DateRen imgUrl={`http://openweathermap.org/img/w/${this.state.icon}.png`} weatherCondition={this.state.reps} place={this.state.place}/>
                    :
                    <div><Web_loader showLoaderp={this.state.showLoader} /> </div>
                }

            </div>

        )
    }
}




