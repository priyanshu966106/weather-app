import * as React from 'react';
import * as ReactDOM from 'react-dom';
import {App} from './components/App'

import './index.css';
import rootReducer from './reducers/weather';
import registerServiceWorker from './registerServiceWorker';
import { createStore } from 'redux';

//import Particles from 'react-particles-js';

//import {Navbar,NavbarBrand,NavItem,NavbarHeader} from 'react-bootstrap'
const store=createStore(rootReducer);
console.log('store.getState()',store.getState());


 
 
ReactDOM.render(
  
  <App/>,
  
  document.getElementById('root') as HTMLElement
);
registerServiceWorker();
